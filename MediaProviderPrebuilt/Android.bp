//
// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "vendor_unbundled_google_modules_MediaProviderGooglePrebuilt_license",
    ],
}

license {
    name: "vendor_unbundled_google_modules_MediaProviderGooglePrebuilt_license",
    visibility: [":__subpackages__"],
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
        "SPDX-license-identifier-BSD",
        "SPDX-license-identifier-ISC",
        "SPDX-license-identifier-MIT",
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_apex_set"],
}

module_apex_set {
    name: "com.google.android.mediaprovider",
    apex_name: "com.android.mediaprovider",
    owner: "google",
    overrides: ["com.android.mediaprovider"],
    filename: "com.google.android.mediaprovider.apex",
    set: "com.google.android.mediaprovider.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    required: [
        "framework-mediaprovider.com.android.mediaprovider",
        "preinstalled-packages-com.google.android.providers.media.module.xml"
    ],
    exported_bootclasspath_fragments: ["com.android.mediaprovider-bootclasspath-fragment"],
}

module_apex_set {
    name: "com.google.android.mediaprovider_compressed",
    apex_name: "com.android.mediaprovider",
    owner: "google",
    overrides: ["com.android.mediaprovider"],
//    filename: "com.google.android.mediaprovider.capex",
    set: "com.google.android.mediaprovider_compressed.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    required: [
        "framework-mediaprovider.com.android.mediaprovider",
        "preinstalled-packages-com.google.android.providers.media.module.xml"
    ],
    exported_bootclasspath_fragments: ["com.android.mediaprovider-bootclasspath-fragment"],
}

prebuilt_etc {
      name: "preinstalled-packages-com.google.android.providers.media.module.xml",
      src: "preinstalled-packages-com.google.android.providers.media.module.xml",
      sub_dir: "sysconfig",
}
