#
# Copyright 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

MAINLINE_USE_LOCAL_DEBUG_MODULES ?= false
ifneq (true,$(MODULE_BUILD_FROM_SOURCE))
ifeq ($(PLATFORM_SDK_VERSION),33)
    $(call inherit-product, vendor/google/modules/build/mainline_modules_t.mk)
else ifeq ($(PLATFORM_SDK_VERSION),32)
    $(call inherit-product, vendor/google/modules/build/mainline_modules_s.mk)
else ifeq ($(PLATFORM_SDK_VERSION),31)
    $(call inherit-product, vendor/google/modules/build/mainline_modules_s.mk)
else ifeq ($(PLATFORM_SDK_VERSION),30)
    $(call inherit-product, vendor/google/modules/build/mainline_modules_r.mk)
else ifeq ($(PLATFORM_SDK_VERSION),29)
    $(call inherit-product, vendor/google/modules/build/mainline_modules_q.mk)
endif

ifeq ($(MAINLINE_USE_LOCAL_DEBUG_MODULES),true)
    # include allows overwriting properties like certificate paths for local test
    include vendor/google/modules/build/mainline_modules_localtest.mk
endif

# Overlays
PRODUCT_PACKAGES += \
    PartnerModulesSettingsOverlay \
    PartnerModulesPermissionControllerOverlay

endif

